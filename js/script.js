// 1 завдання

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsAll = Array.from(new Set([...clients1, ...clients2]))

console.log(clientsAll);

// 2 завдання

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
];

const charactersShortInfo = characters.map((character) => {
    return {
      name: character.name,
      lastName: character.lastName,
      age: character.age
    };
});

console.log(charactersShortInfo);

// 3 завдання

const user = {
    name: "John",
    years: 30
};

const {name: імя, years: вік, isAdmin = false} = user

console.log(імя)
console.log(вік)
console.log(isAdmin)

// 4 завдання

const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}


const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020}

console.log(fullProfile);

// 5 завдання

const books = [
  {
  name: 'Harry Potter',
  author: 'J.K. Rowling'
  }, 

  {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
  }, 

  {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
  }
];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

const allBook = books.concat(bookToAdd)

console.log(allBook);

// 6 завдання

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const newEmployee = Object.assign({}, employee, { age: 30, salary: 10000 });

console.log(newEmployee);
// console.log(employee);

// 7 завдання

// не докумекав як його зробити



  

